# undo the effects of ntp::client
class ntp::purge {
  package { 'ntp':
    ensure => purged
  }

  file { '/etc/init.d/ntp':
    ensure => absent,
  }
  file { '/etc/ntp.conf':
    ensure => absent,
  }
  file { '/etc/default/ntp':
    ensure => absent,
  }

  file { '/var/lib/ntp':
    ensure  => absent,
    recurse => true,
    purge   => true,
    force   => true,
  }
  file { '/etc/ntp.keys.d':
    ensure  => absent,
    recurse => true,
    purge   => true,
    force   => true,
  }

  file { '/usr/local/sbin/ntp-restart-if-required':
    ensure => absent,
  }
}
