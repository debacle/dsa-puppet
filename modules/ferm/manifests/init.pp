# = Class: ferm
#
# This class installs ferm and sets up rules
#
# @param ssh_open_to_world  Set to 'yes' if you want this host to be open to the world.
#                           For now, "maybe" is the default which delegates the decision to
#                           the modules/ferm/templates/me.conf.erb template, which defaults
#                           to open-to-the-world.
#
#                           It is expected that the default change to no at some point.
#                           -- weasel, 2021-05.
#
class ferm(
  Variant[Boolean, Enum['maybe']] $ssh_open_to_world = 'maybe',
) {
  File { mode => '0400' }

  package { 'ferm':
    ensure => installed
  }
  package { 'ulogd2':
    ensure => installed
  }
  package { 'ulogd':
    # Remove instead of purge ulogd because it deletes log files on purge.
    ensure => absent
  }

  service { 'ferm':
    hasstatus => false,
    status    => '/bin/true',
  }
  exec { 'ferm reload':
      command     => 'service ferm reload',
      refreshonly => true,
  }


  $munin_ips = getfromhash($deprecated::nodeinfo, 'misc', 'v4addrs')
    .map |$addr| { "ip_${addr}" }

  munin::check { $munin_ips: script => 'ip_', }

  $munin6_ips = getfromhash($deprecated::nodeinfo, 'misc', 'v6addrs')
    .map |$addr| { "ip_${addr}" }
  munin::ipv6check { $munin6_ips: }

  file { '/etc/ferm':
    ensure  => directory,
    notify  => Exec['ferm reload'],
    require => Package['ferm'],
    mode    => '0755'
  }
  file { '/etc/ferm/dsa.d':
    ensure  => directory,
    mode    => '0555',
    purge   => true,
    force   => true,
    recurse => true,
    notify  => Exec['ferm reload'],
  }
  file { '/etc/ferm/conf.d':
    ensure  => directory,
    mode    => '0555',
    purge   => true,
    force   => true,
    recurse => true,
    notify  => Exec['ferm reload'],
  }
  file { '/etc/default/ferm':
    source  => 'puppet:///modules/ferm/ferm.default',
    require => Package['ferm'],
    notify  => Exec['ferm reload'],
    mode    => '0444',
  }
  file { '/etc/ferm/ferm.conf':
    content => template('ferm/ferm.conf.erb'),
    notify  => Exec['ferm reload'],
  }
  file { '/etc/ferm/conf.d/00-init.conf':
    content => template('ferm/00-init.conf.erb'),
    notify  => Exec['ferm reload'],
  }
  file { '/etc/ferm/conf.d/me.conf':
    content => template('ferm/me.conf.erb'),
    notify  => Exec['ferm reload'],
  }
  file { '/etc/ferm/conf.d/defs.conf':
    content => template('ferm/defs.conf.erb'),
    notify  => Exec['ferm reload'],
  }

  file { '/etc/ferm/conf.d/50-munin-interfaces.conf':
    content => template('ferm/conf.d-munin-interfaces.conf.erb'),
    notify  => Exec['ferm reload'],
  }
  ferm::rule { 'dsa-munin-interfaces-in':
    prio        => '001',
    description => 'munin accounting',
    chain       => 'INPUT',
    domain      => '(ip ip6)',
    rule        => 'daddr ($MUNIN_IPS) NOP'
  }
  ferm::rule { 'dsa-munin-interfaces-out':
    prio        => '001',
    description => 'munin accounting',
    chain       => 'OUTPUT',
    domain      => '(ip ip6)',
    rule        => 'saddr ($MUNIN_IPS) NOP'
  }

  file { '/etc/ferm/dsa.d/010-base.conf':
    content => template('ferm/dsa.d-010-base.conf.erb'),
    notify  => Exec['ferm reload'],
  }

  augeas { 'logrotate_ulogd2':
    context => '/files/etc/logrotate.d/ulogd2',
    changes => [
      'set rule/schedule daily',
      'set rule/delaycompress delaycompress',
      'set rule/rotate 10',
      'set rule/ifempty notifempty',
    ],
  }
  file { '/etc/logrotate.d/ulogd':
    ensure  => absent,
  }
  file { '/etc/logrotate.d/ulogd.dpkg-bak':
    ensure  => absent,
  }
  file { '/etc/logrotate.d/ulogd.dpkg-dist':
    ensure  => absent,
  }

}
