class ferm::ec2 {
  ferm::rule { 'dsa-allow-dhcpv6':
    domain      => 'ip6',
    description => 'allow dhcpv6 traffic',
    table       => 'filter',
    chain       => 'INPUT',
    rule        => 'proto udp saddr fe80::/64 sport 547 daddr fe80::/64 dport 546 jump ACCEPT'
  }
}

