class ferm::per_host {

  case $::hostname {
    czerny,clementi: {
      ferm::rule { 'dsa-upsmon':
        description => 'Allow upsmon access',
        rule        => '&SERVICE_RANGE(tcp, 3493, ( 82.195.75.64/26 192.168.43.0/24 ))'
      }
    }
    default: {}
  }

  # vpn fu
  case $::hostname {
    draghi: {
      ferm::rule { 'dsa-vpn':
        description => 'Allow openvpn access',
        rule        => '&SERVICE(udp, 17257)'
      }
      ferm::rule { 'dsa-routing':
        description => 'forward chain',
        chain       => 'FORWARD',
        rule        => 'policy ACCEPT;
mod state state (ESTABLISHED RELATED) ACCEPT;
interface tun+ ACCEPT;
REJECT reject-with icmp-admin-prohibited
'
      }
      ferm::rule { 'dsa-vpn-mark':
        table => 'mangle',
        chain => 'PREROUTING',
        rule  => 'interface tun+ MARK set-mark 1',
      }
      ferm::rule { 'dsa-vpn-nat':
        table => 'nat',
        chain => 'POSTROUTING',
        rule  => 'outerface !tun+ mod mark mark 1 MASQUERADE',
      }
    }
    ubc-node02,ubc-node03,ubc-node04: {
      ferm::rule { 'dsa-ssh-priv':
        description => 'Allow ssh access',
        rule        => '&SERVICE_RANGE(tcp, 22, ( 172.29.43.240 ))',
      }
    }
    ubc-node-arm04,ubc-node-arm05,ubc-node-arm06: {
      ferm::rule { 'dsa-ssh-priv':
        description => 'Allow ssh access',
        rule        => '&SERVICE_RANGE(tcp, 22, ( 172.29.43.240 ))',
      }
    }
    default: {}
  }
  # tftp
  case $::hostname {
    master: {
      ferm::rule { 'dsa-tftp':
        description => 'Allow tftp access',
        rule        => '&SERVICE_RANGE(udp, 69, ( 82.195.75.64/26 192.168.43.0/24 ))'
      }
    }
    default: {}
  }
}
