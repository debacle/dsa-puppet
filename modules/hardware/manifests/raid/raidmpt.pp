# = Class: hardware::raid::raidmpt
#
# This class installs mpt-status and ensures the daemon is not running
#
# == Sample Usage:
#
#   include hardware::raid::raidmpt
#
class hardware::raid::raidmpt {
	package { 'mpt-status':
		ensure => purged,
	}

	file { '/etc/default/mpt-statusd':
		ensure => absent,
	}
}
