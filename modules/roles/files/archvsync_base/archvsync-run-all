#! /bin/sh

set -e
set -u

BASEDIR=/home/archvsync/
CONFDIR=${BASEDIR}/etc/
BINDIR=${BASEDIR}/bin/
LOGDIR=${BASEDIR}/log/

# Only use a config if it has been synced within this many days.
# archvsync doesn't have a master configuration, so this is a
# best guess as to which configs are actually in use.
OK_AGE=7

sync_archive() {
  local ARCHIVE="${1:+sync:archive:$1}"
  local LOGFILE="ftpsync${1:+-$1}.log.0"

  if [ "$(find "${LOGDIR}/" -type f -name "${LOGFILE}" -mtime -${OK_AGE})" ]; then
    echo Syncing ${ARCHIVE}
    ${BINDIR}/ftpsync ${ARCHIVE}
  else
    echo Skipping ${1:-main config}, too old
  fi
}

if [ ! -e "${LOGDIR}/" ]; then
  echo >&2 Log directoy "${LOGDIR}" not found
  exit 1
fi

if [ ! -e "${CONFDIR}/" ]; then
  echo >&2 Config directoy "${CONFDIR}" not found
  exit 1
fi

if [ ! -e "${BINDIR}/" ]; then
  echo >&2 Script directoy "${BINDIR}" not found
  exit 1
fi

find "${CONFDIR}/" -maxdepth 1 -type f -name "ftpsync*.conf" -printf "%P\n" |
  while read CONFIG; do
    # Extract the "archive name":
    # - transform "ftpsync-foo.conf" to "foo" and "ftpsync.conf"
    #   to an empty string
    CONFIG=${CONFIG%%.conf}
    CONFIG=${CONFIG##ftpsync}
    CONFIG=${CONFIG##-}

    sync_archive $CONFIG
  done
