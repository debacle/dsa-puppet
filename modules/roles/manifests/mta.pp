# Every one of our hosts has an MTA
#
# @param type exim4 or postfix.  exim4 is our default MTA
# @param heavy receive email from the internet and thus do spam filtering etc
# @param mailrelay receive mail on other hosts' behalf.  implies heavy
class roles::mta(
  Enum['exim4', 'postfix'] $type = 'exim4',
  Boolean $heavy = false,
  Boolean $mailrelay = false,
) {
  if $type == 'exim4' {
    if $mailrelay {
      include roles::mailrelay
    } elsif $heavy {
      include exim::mx
    } else {
      include exim
    }
  } elsif $type == 'postfix' {
    if $mailrelay {
      fail("Unsupported: mailrelay on type ${type}")
    }
    include postfix
  } else {
    fail("Unexpected mta type ${type}")
  }


  $mxdata = dig($deprecated::nodeinfo, 'ldap', 'mXRecord')
  $mailport = lookup( { 'name' => 'exim::mail_port', 'default_value' => 25 } )

  if $mxdata and $mxdata.any |$item| { $item =~ /INCOMING-MX/ } {
    # A mail satellite.  Gets mail via the mail relays and sends out mail via the mail relays

    exim::manualroute{ $::fqdn: }

    @@ferm::rule::simple { "submission-from-${::fqdn}":
      tag   => 'smtp::server::submission::to::mail-relay',
      chain => 'submission',
      saddr => $base::public_addresses,
    }

    Ferm::Rule::Simple <<| tag == 'smtp::server::to::mail-satellite' |>> {
      port => $mailport
    }

  } else {
    # not a mail satellite

    if ! defined(Class['exim::mx']) and ! defined(Class['postfix']) {
      fail('We are not an exim::mx (nor a postfix) yet do not have our MXs set to INCOMING-MX.')
    }

    # firewall allow is done by the exim::mx class
  }

  $autocertdir = hiera('paths.auto_certs_dir')
  dnsextras::tlsa_record{ 'tlsa-mailport':
    certfile => "${autocertdir}/${::fqdn}.crt",
    port     => $mailport,
    hostname => $::fqdn,
  }

  $le_dir = hiera('paths.letsencrypt_dir')
  $le_crt_fn = "${le_dir}/${::fqdn}.crt"
  if (file($le_crt_fn, '/dev/null') != '') {
    dnsextras::tlsa_record{ 'tlsa-mailport-le':
      certfile => $le_crt_fn,
      port     => $mailport,
      hostname => $::fqdn,
    }
  }

  ssl::keygen { 'dkim-host-auto': }
  if $facts['ssl_public_keys']['dkim-host-auto'] {
    dnsextras::dkim_record{ 'dkim-host-auto':
      keyfile  => $facts['ssl_public_keys']['dkim-host-auto'],
      selector => 'smtpauto',
      hostname => $::hostname,
      domain   => $::domain,
    }
  }
}
