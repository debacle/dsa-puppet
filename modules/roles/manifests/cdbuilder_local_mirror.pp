class roles::cdbuilder_local_mirror (
  Stdlib::IP::Address $listen_address
) {
  include apache2

  $vhost_listen = "${listen_address}:80"

  apache2::site { '010-local-mirror.cdbuilder.debian.org':
    site    => 'local-mirror.cdbuilder.debian.org',
    content => template('roles/apache-local-mirror.cdbuilder.debian.org.erb'),
  }

  file { '/etc/apache2/ports.conf':
    require => Package['apache2'],
    content => @("EOF"),
        # This file is maintained by puppet
        Listen 127.0.0.1:80
        Listen ${vhost_listen}
        | EOF
    notify  => Service['apache2'],
  }
}
