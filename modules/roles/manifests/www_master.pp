class roles::www_master {
  include apache2

  ssl::service { 'www-master.debian.org':
    notify => Exec['service apache2 reload'],
  }

  $vhost_listen = '*:80'
  $vhost_listen_443 = '*:443'
  $redirect_vhosts = false
  $wwwdo_server_name = 'www-staging.debian.org'
  $wwwdo_document_root = '/srv/www.debian.org/www'
  apache2::site { '005-www-staging.debian.org':
    site    => 'www-staging.debian.org',
    content => template('roles/apache-www.debian.org.erb'),
  }

  ssl::service { 'www-staging.debian.org' :
    notify => Exec['service apache2 reload'],
  }

  # tell the mail-relays to forward this domain to us
  exim::manualroute{ 'www-master.debian.org': }
  # and then handle it locally
  exim::vdomain { 'www-master.debian.org':
    owner   => 'debwww',
    group   => 'debwww',
    maildir => '/srv/www.debian.org/mail',
  }
}
