# sources.debian.org role

# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::debsources (
  String  $db_address,
  Integer $db_port,
) {
  include apache2
  include apache2::ssl

  package { 'libapache2-mod-wsgi-py3': ensure => installed, }

  apache2::site { 'sources.debian.org':
    site   => 'sources.debian.org',
    source => 'puppet:///modules/roles/debsources/sources.debian.org.conf',
  }
  ssl::service { 'sources.debian.org':
    notify => Exec['service apache2 reload'],
  }

  @@postgres::cluster::hba_entry { "debsources-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => 'debsources',
    user     => ['debsource_admin', 'debsource_updater'],
    address  => $base::public_addresses,
  }

  @@postgres::cluster::hba_entry { "debsources-guest-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => 'debsources',
    user     => ['guest'],
    method   => 'trust',
    address  => $base::public_addresses,
  }
}
