# buildhost for dsa-wiki
class roles::dsa_wiki_buildhost {
  ssh::authorized_key_collect { 'dsa_wiki_buildhost':
    target_user => 'dsa',
    collect_tag => 'dsa_wiki_buildhost',
  }
}
