# the sink where all provider of webserver logs ship their things to
class roles::weblog_destination {
  ssh::authorized_key_collect { 'weblogsync':
    target_user => 'weblogsync',
    collect_tag => 'weblogsync',
  }
}
