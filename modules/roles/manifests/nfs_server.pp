# NFS server role
#
# @param allow_client_access  IP addresses from which to accept connections. defaults to IPv4 localnet
class roles::nfs_server (
  Array[Stdlib::IP::Address] $allow_client_access = ['127.0.0.0/8'],
) {

  package { [
      'nfs-common',
      'nfs-kernel-server'
    ]:
    ensure => installed
  }

  service { 'nfs-common':
    hasstatus => false,
    status    => '/bin/true',
  }
  service { 'nfs-kernel-server':
    hasstatus => false,
    status    => '/bin/true',
  }

  $clients = join_spc(filter_ipv4($allow_client_access))

  ferm::rule { 'dsa-portmap':
    description => 'Allow portmap access',
    rule        => sprintf('&TCP_UDP_SERVICE_RANGE(111, (%s))', $clients)
  }
  ferm::rule { 'dsa-nfs':
    description => 'Allow nfsd access',
    rule        => sprintf('&TCP_UDP_SERVICE_RANGE(2049, (%s))', $clients)
  }
  ferm::rule { 'dsa-status':
    description => 'Allow statd access',
    rule        => sprintf('&TCP_UDP_SERVICE_RANGE(10000, (%s))', $clients)
  }
  ferm::rule { 'dsa-mountd':
    description => 'Allow mountd access',
    rule        => sprintf('&TCP_UDP_SERVICE_RANGE(10002, (%s))', $clients)
  }
  ferm::rule { 'dsa-lockd':
    description => 'Allow lockd access',
    rule        => sprintf('&TCP_UDP_SERVICE_RANGE(10003, (%s))', $clients)
  }

  file { '/etc/default/nfs-common':
    source => 'puppet:///modules/roles/nfs_server/nfs-common.default',
    before => Package['nfs-common'],
    notify => Service['nfs-common'],
  }
  file { '/etc/default/nfs-kernel-server':
    source => 'puppet:///modules/roles/nfs_server/nfs-kernel-server.default',
    before => Package['nfs-kernel-server'],
    notify => Service['nfs-kernel-server'],
  }
  file { '/etc/modprobe.d/lockd.local':
    source => 'puppet:///modules/roles/nfs_server/lockd.local.modprobe',
    before => Package['nfs-common'],
    notify => Service['nfs-common'],
  }

  munin::check { ['nfsd', 'nfsd4']: }
}
