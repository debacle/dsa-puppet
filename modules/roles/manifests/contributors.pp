# contributors.debian.org role
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::contributors (
  String  $db_address,
  Integer $db_port,
) {
  include apache2
  include roles::sso_rp

  ssl::service { 'contributors.debian.org':
    notify => Exec['service apache2 reload'],
  }

  @@postgres::cluster::hba_entry { "contributors-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => ['contributors'],
    user     => ['contributors', 'contributorsweb'],
    address  => $base::public_addresses,
  }

  dsa_systemd::linger { 'contributors': }
}
