# onion.debian.org website build
#
class roles::onion_master {
  file { '/srv/puppet.debian.org':
    ensure => directory
  }
  file { '/srv/puppet.debian.org/puppet-facts':
    ensure => directory
  }
  concat { '/srv/puppet.debian.org/puppet-facts/onionbalance-services.yaml':
    notify => Exec['rebuild-onion-website'],
  }
  Concat::Fragment <<| tag == 'onionbalance-services.yaml' |>>
  concat { '/srv/puppet.debian.org/puppet-facts/onionbalancev3-services.yaml':
    notify => Exec['rebuild-onion-website'],
  }
  Concat::Fragment <<| tag == 'onionbalancev3-services.yaml' |>>

  exec { 'rebuild-onion-website':
    command     => '/bin/su - staticsync -c \'make -C /srv/onion-master.debian.org\'',
    refreshonly => true,
  }
}
