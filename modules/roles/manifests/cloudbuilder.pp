# basic setup to build cloud images
class roles::cloudbuilder {
  file { '/srv/cloudbuilder.debian.org':
    ensure => directory,
    owner  => 'cloud-build',
    group  => 'cloud-build',
    mode   => '2775',
  }
  file { '/home/cloud-build':
    ensure => link,
    target => '/srv/cloudbuilder.debian.org',
  }

  dsa_systemd::linger { 'cloud-build': }

  exec { 'add-cloud-build-user-to-kvm':
    command => 'adduser cloud-build kvm',
    onlyif  => "getent group kvm > /dev/null && ! getent group kvm | grep '\\<cloud-build\\>' > /dev/null"
  }

 if (versioncmp($::lsbmajdistrelease, '11') >= 0) {
   ensure_packages ( [
     'podman',
   ], { ensure => 'installed' })
 }

 # On bookworm and bullseye vsock is already supported but the udev rule is
 # missing. On trixie the udev rule is shipped by default.
 file { '/etc/udev/rules.d/99-cloudbuilder.rules':
   ensure  => $::lsbmajdistrelease ? {
     /(11|12)/ => file,
     default   => absent,
   },
   content => @(EOT)
     KERNEL=="vhost-vsock", GROUP="kvm", MODE="0660", OPTIONS+="static_node=vhost-vsock"
     | EOT
 }
}
