# deriv.debian.net -- deriv census
class roles::deriv {
  include apache2
  package { 'debian.org-deriv.debian.net': ensure => installed, }
}
