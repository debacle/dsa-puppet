# origin of bugs.debian.org mirroring to bugs_mirror nodes.
# @param user  username of the debbugs mirror user
class roles::bugs_mirror_master (
  String $user = 'debbugs-mirror',
) {
  file { "/home/${user}":
    ensure => 'directory',
    owner  => $user,
    group  => $user,
    mode   => '2755',
  }
  file { "/home/${user}/bin":
    ensure => 'directory',
    owner  => $user,
    group  => $user,
    mode   => '2755',
  }
  file { "/home/${user}/bin/smartsync":
    source => 'puppet:///modules/roles/bugs_mirror_master/smartsync',
    owner  => $user,
    group  => $user,
    mode   => '0555',
  }

  file { "/home/${user}/etc":
    ensure => 'directory',
    owner  => $user,
    group  => $user,
    mode   => '2755',
  }
  file { "/home/${user}/etc/smartsync.conf":
    source => 'puppet:///modules/roles/bugs_mirror_master/smartsync.conf',
    owner  => $user,
    group  => $user,
  }
  file { "/home/${user}/etc/smartsync.fullsync-list":
    source => 'puppet:///modules/roles/bugs_mirror_master/smartsync.fullsync-list',
    owner  => $user,
    group  => $user,
  }


  ssh::keygen {$user: }
  ssh::authorized_key_add { "bugs_mirror_master::${user}":
    target_user => 'debbugs',
    command     => '/usr/local/bin/bugs-ssh-command-wrap',
    key         => dig($facts, 'ssh_keys_users', $user, 'id_rsa.pub', 'line'),
    collect_tag => 'bugs-mirror.debian.org',
  }


  file { "/home/${user}/.ssh/config":
    content => @("EOF"),
                 ControlPersist 1800
                 ServerAliveInterval 300
                 ControlMaster auto
                 ControlPath /home/${user}/.ssh/.sock.%C
              | EOF
  }


  dsa_systemd::linger { $user: }
  file { "/home/${user}/.config":
    ensure => 'directory',
    owner  => $user,
    group  => $user,
    mode   => '2755',
  }
  file { "/home/${user}/.config/systemd":
    ensure => 'directory',
    owner  => $user,
    group  => $user,
    mode   => '2755',
  }
  file { "/home/${user}/.config/systemd/user":
    ensure => 'directory',
    owner  => $user,
    group  => $user,
    mode   => '2755',
  }
  file { "/home/${user}/.config/systemd/user/default.target.wants":
    ensure => 'directory',
    owner  => $user,
    group  => $user,
    mode   => '2755',
  }
  file { "/home/${user}/.config/systemd/user/smartsync.service.wants":
    ensure  => 'directory',
    owner   => $user,
    group   => $user,
    mode    => '2755',
    purge   => true,
    force   => true,
    recurse => true,
  }


  file { "/home/${user}/.config/systemd/user/smartsync.service":
    source => 'puppet:///modules/roles/bugs_mirror_master/smartsync.service',
    owner  => $user,
    group  => $user,
  }
  file { "/home/${user}/.config/systemd/user/smartsync@.service":
    source => 'puppet:///modules/roles/bugs_mirror_master/smartsync@.service',
    owner  => $user,
    group  => $user,
  }

  file { "/home/${user}/.config/systemd/user/default.target.wants/smartsync.service":
    ensure => link,
    target => '../smartsync.service',
  }
  # Note, services will not auto-reload or auto-start
  File<<| tag == 'bugs_mirror_to_bugs_mirror_master' |>>
}
