class roles::piuparts {
  include apache2
  ssl::service { 'piuparts.debian.org':
    notify => Exec['service apache2 reload'],
  }
  include apache2::ssl

  ssh::authorized_key_collect { 'piupartsm':
    target_user => 'piupartsm',
    collect_tag => 'roles::piuparts_runner::to::piuparts',
  }
}
