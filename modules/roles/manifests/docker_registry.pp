class roles::docker_registry {
  include apache2
  include apache2::auth_digest
  include apache2::authn_file
  include apache2::proxy_http
  include apache2::rewrite
  include apache2::ssl

  ensure_packages ( [
    'docker-registry',
    ], {
    ensure => 'installed',
  })

  ssl::service { 'docker-registry.debian.org':
    notify => Exec['service apache2 reload'],
  }
  ssl::service { 'docker-registry-priv.debian.org':
    notify => Exec['service apache2 reload'],
  }

  apache2::site { 'docker-registry.debian.org':
    ensure => 'present',
    site   => 'docker-registry.debian.org.conf',
    source => 'puppet:///modules/roles/docker_registry/apache.conf',
  }


  $pw_salt = hkdf('/etc/puppet/secret', "docker-registry-${::hostname}-salt-generator")
  $container_builder_01_password = hkdf('/etc/puppet/secret', "docker-registry:${::fqdn}:container-builder-01.debian.org")
  $container_builder_01_password_crypted = pw_hash($container_builder_01_password, 'SHA-512', $pw_salt)

  file { '/etc/apache2/docker-registry-priv.debian.org.htpasswd':
    mode      => '0440',
    owner     => 'root',
    group     => 'www-data',
    # security sensitive, don't show in logs
    show_diff => false,
    content   => @("EOF"),
        container-builder-01:$container_builder_01_password_crypted
        | EOF
  }
}
