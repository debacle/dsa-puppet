#
# bacula storage node
#
class roles::bacula::storage(
) {
  include bacula::storage

  $pg_server = lookup('bacula::director::db_address')
  $pg_port   = lookup('bacula::director::db_port')

  @@postgres::cluster::hba_entry { 'bacula-sd':
    tag      => "postgres::cluster::${pg_port}::hba::${pg_server}",
    pg_port  => $pg_port,
    database => 'bacula',
    user     => "bacula-${::hostname}-reader",
    address  => $base::public_addresses,
  }
}
