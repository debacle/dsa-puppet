# The sreview class.  sreview is the conference talk video reviewing service
#
# @param user         username of the sreview role user
# @param db_address   hostname of the postgres server for this service
# @param db_port      port of the postgres server for this service
# @param encoders     optional list of IP addresses of encoder machines, to allow
#                     database access
class roles::sreview (
  String $user = 'sreview',
  Optional[String] $db_address = undef,
  Optional[Integer] $db_port = undef,
  Optional[Variant[Stdlib::IP::Address, Array[Stdlib::IP::Address]]] $encoders = undef,
) {
  include apache2
  ssl::service { 'sreview.debian.net': notify  => Exec['service apache2 reload'], }

  dsa_systemd::linger { $user: }

  # enable copying files from the sreview host to the video_archive host.
  ssh::keygen { $user: }
  ssh::authorized_key_add { "sreview_to_video_archive::${user}":
    target_user => 'videoteam',
    command     => 'bin/ssh-from-sreview-wrap',
    key         => dig($facts, 'ssh_keys_users', $user, 'id_rsa.pub', 'line'),
    collect_tag => 'sreview_to_video_archive',
  }

  if $encoders {
    if ! $db_address or ! $db_port {
      fail('Must specify database server and port if encoders are specified!')
    }
    @@postgres::cluster::hba_entry { 'sreview-remote-encoders':
      tag      => "postgres::cluster::${db_port}::hba::${db_address}",
      pg_port  => $db_port,
      database => ['sreview'],
      user     => ['sreview'],
      address  => $encoders,
    }
  }
}
