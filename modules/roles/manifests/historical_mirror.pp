# a mirror for archive.debian.org
# @param sslname provide rsync via ssl as well
# @param listen_addr IP addresses to have apache listen on
class roles::historical_mirror(
  Optional[String] $sslname = undef,
  Array[Stdlib::IP::Address] $listen_addr = [],
){
  include roles::archvsync_base

  include apache2
  include apache2::expires
  include apache2::ssl

  $enclosed_addresses_rsync = empty($listen_addr) ? {
    true    => ['[::]'],
    default => enclose_ipv6($listen_addr),
  }
  $_enclosed_addresses = empty($listen_addr) ? {
    true    => ['*'],
    default => enclose_ipv6($listen_addr),
  }
  $vhost_listen = $_enclosed_addresses.map |$a| { "${a}:80" } .join(' ')
  $vhost_listen_443 = $_enclosed_addresses.map |$a| { "${a}:443" } .join(' ')
  $mirror_basedir_prefix = hiera('role_config__mirrors.mirror_basedir_prefix')
  $archive_root = "${mirror_basedir_prefix}debian-archive"

  apache2::site { '010-archive.debian.org':
    site    => 'archive.debian.org',
    content => template('roles/apache-archive.debian.org.erb'),
  }

  apache2::site { '015-rsync.archive.debian.org':
    site    => 'rsync.archive.debian.org',
    content => template('roles/historical_mirror/rsync.archive.debian.org.erb')
  }

  ssl::service { 'archive.debian.org':
    notify => Exec['service apache2 reload'],
  }

  if $sslname {
    ssl::service { $sslname:
      tlsaport => [],
    }
  }
  rsync::site { 'archive':
    content     => template('roles/historical_mirror/rsyncd.conf.erb'),
    max_clients => 100,
    sslname     => $sslname,
    binds       => $enclosed_addresses_rsync,
  }

  Ferm::Rule::Simple <<| tag == 'ssh::server::from::historical_master' |>>
}
