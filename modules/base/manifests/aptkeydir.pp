# create and maintain etc/apt/puppet-keys.d
class base::aptkeydir (
) {
  file { '/etc/apt/puppet-keys.d':
    ensure  => directory,
    recurse => true,
    purge   => true,
    force   => true,
  }
}
