# bacula class -- defines all the variables we care about in our bacula deployment
#
# @param ssl_ca_path      full path and filename specifying a PEM encoded TLS CA certificate(s)
# @param ssl_client_cert  path to TLS client certificate
# @param ssl_client_key   path to TLS client certificate key
# @param ssl_server_cert  path to TLS server certificate
# @param ssl_server_key   path to TLS server certificate key
# @param email_all        email address for all reports
# @param email_error      email address for errors
# @param email_operator   email address for the operator (to mount tapes etc)
# @param email_daemon     email address for messages from the daemon
# @param public_addresses this host's public IP addresses.  The ones it connects out from and is reachable from outsite.
# @param has_ipv4         daemons should listen on ipv4
# @param has_ipv6         daemons should listen on ipv6
class bacula (
  String $ssl_ca_path,
  String $ssl_client_cert,
  String $ssl_client_key,
  String $ssl_server_cert,
  String $ssl_server_key,

  Optional[String] $email_all = undef,
  # default to all if defined, otherwise default to 'root' to enforce error delivery
  String $email_error                          = $email_all ? { undef => 'root', default => $email_all },
  String $email_operator                       = $email_error,
  String $email_daemon                         = $email_error,
  Array[Stdlib::IP::Address] $public_addresses = $base::public_addresses,
  Boolean $has_ipv4                            = $bacula::public_addresses.any |$addr| { $addr =~ Stdlib::IP::Address::V4 },
  Boolean $has_ipv6                            = $bacula::public_addresses.any |$addr| { $addr =~ Stdlib::IP::Address::V6 },
) {
  # This file is used by our helper scripts on the director
  $bacula_dsa_client_list     = '/etc/bacula/dsa-clients'
  $tag_bacula_dsa_client_list = 'bacula::dsa::clientlist'

  $bacula_tls_ca_certificate_file = "TLS CA Certificate File = \"${ssl_ca_path}\""
  $bacula_tls_client_certificate  = "TLS Certificate = \"${ssl_client_cert}\""
  $bacula_tls_client_key          = "TLS Key = \"${ssl_client_key}\""
  $bacula_tls_server_certificate  = "TLS Certificate = \"${ssl_server_cert}\""
  $bacula_tls_server_key          = "TLS Key = \"${ssl_server_key}\""

  file { '/usr/local/sbin/bacula-idle-restart':
    mode   => '0555',
    source => 'puppet:///modules/bacula/bacula-idle-restart',
  }
}
