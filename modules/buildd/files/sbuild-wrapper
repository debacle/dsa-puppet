#!/bin/bash

##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

# Default values in sbuild
ARCH="$(dpkg --print-architecture)"
DIST="sid"

# Parse all the arguments to get the architecture and the distribution
while [ "$1" ]; do
    case "$1" in
        --arch=*)
            ARCH="${1#--arch=}"
            ;;
        --dist=*)
            DIST="${1#--dist=}"
            ;;
        -d)
            DIST="$2"
            ARGS+=("$1")
            shift
            ;;
        *)
            ;;
    esac
    ARGS+=("$1")
    shift
done

# Use the traditional schroot mode for older distributions (excluding -backports on purpose)
if [ "${DIST}" = "bullseye-security" ] || [ "${DIST}" = "bookworm-security" ] || [ "${DIST}" = "bookworm" ] ; then
    exec /usr/bin/sbuild --chroot-mode=schroot "${ARGS[@]}"
fi

# Split the distribution in base + variant
case ${DIST} in
    experimental)
        SUITE_BASE="sid"
        SUITE_VARIANT="experimental"
        ;;
    *-*-*)
        SUITE_BASE=$(echo "${DIST}" | cut -f1 -d-)
        SUITE_VARIANT=$(echo "${DIST}" | cut -f2,3 -d-)
        ;;
    *-*)
        SUITE_BASE=$(echo "${DIST}" | cut -f1 -d-)
        SUITE_VARIANT=$(echo "${DIST}" | cut -f2 -d-)
        ;;
    *)
        SUITE_BASE=$(echo "${DIST}" | cut -f1 -d-)
        if [ "${SUITE_BASE}" != "sid" ]; then
            SUITE_VARIANT="proposed-updates"
        fi
        ;;
esac

# Support for local mirror
if [ -f /etc/schroot/dsa/default-mirror ]; then
    DEBIAN_MIRROR=$(cat /etc/schroot/dsa/default-mirror)
fi

# Helper function handling deb & deb-src + mirror
function repositories_add_entry() {
    local mirror="$1"
    local suite="$2"
    local component="$3"
    local component_src="$4"

    if [ -n "${DEBIAN_MIRROR}" ] && [ "${mirror}" = "https://deb.debian.org/debian" ]; then
        REPOSITORIES+=("--extra-repository=deb ${DEBIAN_MIRROR} ${suite} ${component}")
        REPOSITORIES+=("--extra-repository=deb-src ${DEBIAN_MIRROR} ${suite} ${component_src}")
    fi

    REPOSITORIES+=("--extra-repository=deb ${mirror} ${suite} ${component}")
    REPOSITORIES+=("--extra-repository=deb-src ${mirror} ${suite} ${component_src}")
}

# Compute the extra repositories to add
COMPONENT="main contrib"
COMPONENT_SRC="main contrib non-free"
if [ "${SUITE_BASE}" != "bullseye" ] ; then
    COMPONENT_SRC="${COMPONENT_SRC} non-free-firmware"
fi
repositories_add_entry "https://deb.debian.org/debian" "${SUITE_BASE}" "${COMPONENT}" "${COMPONENT_SRC}"

case "${SUITE_VARIANT}" in
    "") # e.g. sid
        repositories_add_entry "https://incoming.debian.org/debian-buildd" "buildd-${SUITE_BASE}" "${COMPONENT}" "${COMPONENT_SRC}"
        ;;
    backports-sloppy)
        repositories_add_entry "https://deb.debian.org/debian" "${SUITE_BASE}-backports" "${COMPONENT}" "${COMPONENT_SRC}"
        repositories_add_entry "https://incoming.debian.org/debian-buildd" "buildd-${SUITE_BASE}-backports" "${COMPONENT}" "${COMPONENT_SRC}"
        ;& # fallthrough
    backports | proposed-updates)
        repositories_add_entry "https://deb.debian.org/debian" "${SUITE_BASE}-${SUITE_VARIANT}" "${COMPONENT}" "${COMPONENT_SRC}"
        repositories_add_entry "https://incoming.debian.org/debian-buildd" "buildd-${SUITE_BASE}-${SUITE_VARIANT}" "${COMPONENT}" "${COMPONENT_SRC}"
        ;;
    experimental)
        repositories_add_entry "https://deb.debian.org/debian" "${SUITE_VARIANT}" "${COMPONENT}" "${COMPONENT_SRC}"
        repositories_add_entry "https://incoming.debian.org/debian-buildd" "buildd-${SUITE_BASE}" "${COMPONENT}" "${COMPONENT_SRC}"
        repositories_add_entry "https://incoming.debian.org/debian-buildd" "buildd-${SUITE_VARIANT}" "${COMPONENT}" "${COMPONENT_SRC}"
        ;;
    security)
        if [ "${SUITE_BASE}" != "buster" ] ; then
            SUITE_SECURITY="-security"
        else
            SUITE_SECURITY="/updates"
        fi
        repositories_add_entry "https://security-master.debian.org/debian-security" "${SUITE_BASE}${SUITE_SECURITY}" "${COMPONENT}" "${COMPONENT_SRC}"
        repositories_add_entry "https://security-master.debian.org/debian-security-buildd" "buildd-${SUITE_BASE}${SUITE_SECURITY}" "${COMPONENT}" "${COMPONENT_SRC}"

        # Temporarily add proposed-updates for bullseye-security and bookworm-security chroots
        # for e.g. openjdk-11 and firefox-esr/thunderbird updates (see RT#9257)
        if [ "${SUITE_BASE}" = "bullseye" ] || [ "${SUITE_BASE}" = "bookworm" ] ; then
            repositories_add_entry "https://deb.debian.org/debian" "${SUITE_BASE}-proposed-updates" "${COMPONENT}" "${COMPONENT_SRC}"
        fi
        ;;
esac

# Compute the chroot file to use
CHROOTFILE="/srv/chroot/${SUITE_BASE}_${ARCH}.tar.gz"
if ! test -f "$CHROOTFILE" ; then
    echo "error: chroot file $CHROOTFILE missing"
    exit 1
fi

PACKAGE=$(echo "${ARGS[-1]}" | cut -f1 -d_)
case "${PACKAGE}" in
    # Those packages need network access during build
    debian-installer | debian-installer-netboot-images)
        network="--enable-network"
        ;;
    *)
        network="--no-enable-network"
        ;;
esac

exec /usr/bin/sbuild --chroot-mode=unshare "$network" --chroot="$CHROOTFILE" "${REPOSITORIES[@]}" "${ARGS[@]}"
