# Please contact the RTC team about this service at debian-rtc-team@alioth-lists.debian.net
#

class profile::prosody {

  include profile::prosody::packages

  # get the users in the debvoip group from a fact, append @debian.org and save it as $admins
  $admins = $facts['local_groups']['debvoip']['mem'].map |$x| { "\"${x}@debian.org\"" }

  ensure_packages(['prosody', 'prosody-modules'])

  file { '/etc/prosody/prosody.cfg.lua':
    ensure  => file,
    content => template('profile/prosody/prosody.cfg.lua.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    require => [Package['prosody'], Package['prosody-modules']],
    notify  => Service['prosody'],
  }

  service { 'prosody':
    ensure  => running,
    enable  => true,
    require => [Package['prosody'], Package['prosody-modules']],
  }

  file { '/etc/prosody/conf.avail/debian.org.cfg.lua':
    ensure  => file,
    content => template('profile/prosody/conf.avail/debian.org.cfg.lua.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    require => [Package['prosody'], Package['prosody-modules']],
    notify  => Service['prosody'],
  }

  file { '/etc/prosody/conf.d':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0555',
    recurse => true,
    purge   => true,
    require => [Package['prosody'], Package['prosody-modules']],
  }

  file { '/etc/prosody/conf.d/debian.org.cfg.lua':
    ensure => link,
    target => '/etc/prosody/conf.avail/debian.org.cfg.lua',
  }

  $prosody_directories = [ '/srv/prosody', '/srv/prosody/antispam' ]

  $prosody_directories.each |$index, $directory| {
    file { $directory:
      ensure => directory,
      owner  => 'root',
      group  => 'debvoip',
      mode   => '2775',
    }
  }

}
