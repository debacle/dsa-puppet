# LVM config for the ppc hosts that make up ganeti3.conova.debian.org
class profile::lvm::ganeti3_conova {
  class { 'dsalvm':
    global_filter  => '[ "a|^/dev/md[0-9]*$|", "r/.*/" ]',
  }
}
