# LVM config for the arm hosts that make up ganeti4.ubc.debian.org
class profile::lvm::ganeti4_ubc {
  class { 'dsalvm':
    global_filter  => '[ "a|^/dev/md[0-9]*$|", "r/.*/" ]',
    issue_discards => true,
  }
}
