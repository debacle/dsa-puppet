class nagios::client inherits nagios {
  package { 'dsa-nagios-nrpe-config':
    ensure => purged
  }
  package { 'dsa-nagios-checks':
    ensure => installed,
    tag    => extra_repo,
  }

  service { 'nagios-nrpe-server':
    ensure    => running,
    hasstatus => false,
    pattern   => 'nrpe',
  }

  Ferm::Rule::Simple <<| tag == 'nagios-nrpe::server' |>>

  file { '/etc/default/nagios-nrpe-server':
    source  => 'puppet:///modules/nagios/common/default',
    require => Package['nagios-nrpe-server'],
    notify  => Service['nagios-nrpe-server'],
  }
  file { '/etc/default/nagios-nrpe':
    ensure => absent,
    notify => Service['nagios-nrpe-server'],
  }
  file { '/etc/nagios/':
    ensure  => directory,
    recurse => remote,
    source  => 'puppet:///files/empty/',
    require => Package['nagios-nrpe-server'],
    notify  => Service['nagios-nrpe-server'],
  }
  file { '/etc/nagios/nrpe.cfg':
    content => template('nagios/nrpe.cfg.erb'),
    notify  => Service['nagios-nrpe-server'],
  }
  file { '/etc/nagios/nrpe.d':
    ensure  => directory,
    recurse => remote,
    source  => 'puppet:///files/empty/',
    notify  => Service['nagios-nrpe-server'],
  }

  concat { '/etc/nagios/nrpe.d/debianorg.cfg':
    ensure_newline => true,
    warn           => '# This file is maintained with puppet',
    notify         => Service['nagios-nrpe-server'],
    mode           => '0444',
  }
  concat::fragment { 'nrpe-debian-staticchecks':
    target  => '/etc/nagios/nrpe.d/debianorg.cfg',
    content => template('nagios/inc-debian.org.erb'),
  }
  Concat::Fragment <<| tag == 'nagios-nrpe::server::debianorg.cfg' |>>

  file { '/etc/nagios/nrpe.d/nrpe_dsa.cfg':
    source => 'puppet:///modules/nagios/dsa-nagios/generated/nrpe_dsa.cfg',
    notify => Service['nagios-nrpe-server'],
  }
  file { '/etc/nagios/obsolete-packages-ignore':
    source  => 'puppet:///modules/nagios/common/obsolete-packages-ignore',
    require => Package['dsa-nagios-checks'],
  }
  file { '/etc/nagios/check-libs.conf':
    source  => 'puppet:///modules/nagios/common/check-libs.conf',
    require => Package['dsa-nagios-checks'],
  }
  file { '/etc/nagios/obsolete-packages-ignore.d/hostspecific':
    content => template('nagios/obsolete-packages-ignore.d-hostspecific.erb'),
    require => Package['dsa-nagios-checks'],
  }
  file { '/usr/local/sbin/dsa-check-libs':
    ensure => absent,
  }

  file { '/etc/cron.d/puppet-nagios-wraps': ensure => absent, }
  concat::fragment { 'puppet-crontab--nagios--dsa-check-puppet-agent':
    target  => '/etc/cron.d/puppet-crontab',
    order   => '010',
    content => @(EOF)
      47 * * * * root dsa-wrap-nagios-check -s puppet-agent dsa-check-puppet_agent -d0 -c 28800 -w 18000
      | EOF
  }
}
