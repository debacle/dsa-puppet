# munin master node
class munin::master {
  include apache2

  package { 'munin':
    ensure => installed
  }

  ssh::keygen { 'munin' : }
  ssh::authorized_key_add { 'munin-async-fetcher':
    target_user => 'munin-async',
    key         => dig($facts, 'ssh_keys_users', 'munin', 'id_rsa.pub', 'line'),
    command     => '/usr/share/munin/munin-async --spoolfetch',
    from        => $base::public_addresses,
    collect_tag => 'munin::munin-async-fetch'
  }

  file { '/etc/munin/munin.conf':
    content => template('munin/munin.conf.erb'),
    require => Package['munin'];
  }

  ssl::service { 'munin.debian.org':
    notify => Exec['service apache2 reload'],
  }
  file { '/etc/munin/munin-conf.d':
    ensure  => directory,
    mode    => '0755',
    purge   => true,
    force   => true,
    recurse => true,
    source  => 'puppet:///files/empty/',
  }

  Munin::Master_per_node<<| |>>

  file { '/etc/cron.d/puppet-munin-cleanup': ensure => absent; }

  concat::fragment { 'puppet-crontab--munin-master':
    target => '/etc/cron.d/puppet-crontab',
    source => 'puppet:///modules/munin/master-cleanup-cron',
  }
}
