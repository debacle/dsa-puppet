class tcp_bbr {
  base::linux_module { 'tcp_bbr': }
  base::linux_module { 'sch_fq': }

  debian_org::sysctl::value {
    'net.core.default_qdisc'         : value => 'fq';
    'net.ipv4.tcp_congestion_control': value => 'bbr';
  }
}
