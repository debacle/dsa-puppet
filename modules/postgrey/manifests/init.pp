class postgrey {

	package { 'postgrey':
		ensure => installed
	}

	service { 'postgrey':
		ensure  => running,
		require => Package['postgrey']
	}

	file { '/etc/default/postgrey':
		source  => 'puppet:///modules/postgrey/default',
		require => Package['postgrey'],
		notify  => Service['postgrey']
	}

	file { '/etc/postgrey/whitelist_clients.local':
		source  => 'puppet:///modules/postgrey/whitelist_clients.local',
		require => Package['postgrey'],
		notify  => Service['postgrey']
	}

	file { '/etc/tmpfiles.d/postgrey.conf':
		content => 'd	/var/run/postgrey	0750	postgrey	Debian-exim	-	-',
	}
}
