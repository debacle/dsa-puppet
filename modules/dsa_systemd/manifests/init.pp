class dsa_systemd {
	file { '/etc/systemd/journald.conf':
		source => 'puppet:///modules/dsa_systemd/journald.conf',
	}

	dsa_systemd::mask { 'sys-kernel-debug-tracing.mount': }
	dsa_systemd::mask { 'sys-kernel-debug.mount': }

	file { '/etc/profile.d/dsa-dbus-envs.sh':
		content  => @(EOF),
				# Set XDG_RUNTIME_DIR and DBUS_SESSION_BUS_ADDRESS in login shells
				# so systemctl --user actually works even after su and sudo.
				if [ -z "${XDG_RUNTIME_DIR:-}" ] ; then
				  export XDG_RUNTIME_DIR="/run/user/$(id -u)"
				fi
				if [ -z "${DBUS_SESSION_BUS_ADDRESS:-}" ] ; then
				  export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"
				fi
				| EOF
	}

	include stretch::network_online

	if $::hostname == 'godard' {
		exec {'mkdir -p /etc/systemd/journald.conf.d':
			unless => 'test -d /etc/systemd/journald.conf.d',
		}
		file { '/etc/systemd/journald.conf.d/persistency.conf':
			source => 'puppet:///modules/dsa_systemd/persistency.conf',
		}
	}

	file { '/usr/local/sbin/systemd-cleanup-failed':
		source => 'puppet:///modules/dsa_systemd/systemd-cleanup-failed',
		mode   => '0555',
	}
	concat::fragment { 'puppet-crontab--systemd-cleanup-failed':
		target => '/etc/cron.d/puppet-crontab',
		content  => @("EOF"),
				*/10 * * * * root /usr/local/sbin/systemd-cleanup-failed
				| EOF
	}

	# systemd bug on buster, maybe related to https://github.com/systemd/systemd/issues/12401
	# | root@onionoo-backend-01:~# loginctl enable-linger onionoo
	# | Could not enable linger: No such file or directory
	# Having the /var/lib/systemd/linger directory exist already enables
	# us to just define file resources in dsa_systemd::linger.  That won't
	# start the user session immediately, but it'll work after a reboot
	file { '/var/lib/systemd/linger':
		mode   => '0755',
		ensure => 'directory',
	}
}
