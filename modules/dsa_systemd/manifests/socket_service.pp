# define and enable (or disable) a .socket activated .service
#
define dsa_systemd::socket_service(
  Enum['present','absent'] $ensure = 'present',
  String $service_content,
  String $socket_content,
) {
  $ensure_service = $ensure ? {
    present => running,
    absent  => stopped,
  }

  $ensure_enable = $ensure ? {
    present => true,
    absent  => false,
  }

  $systemd_service = "${name}.socket"
  $service_file = "/etc/systemd/system/${name}@.service"
  $socket_file = "/etc/systemd/system/${systemd_service}"

  # if we enable the service, we want the files before the service, so we
  #   subscribe the service to the files.
  # if we remove the service, we want the service disabled before the files
  #   go away, so we say the service needs the files to be handled before.
  $service_before = $ensure ? {
    present => [],
    default => [
      File[$service_file],
      File[$socket_file],
    ],
  }
  $service_subscribe = $ensure ? {
    present => [
      File[$service_file],
      File[$socket_file],
    ],
    default => [],
  }


  file { $service_file:
    ensure  => $ensure,
    content => $service_content,
    notify  => Exec['systemctl daemon-reload'],
  }

  file { $socket_file:
    ensure  => $ensure,
    content => $socket_content,
    notify  => Exec['systemctl daemon-reload'],
  }

  service { $systemd_service:
    ensure    => $ensure_service,
    enable    => $ensure_enable,
    notify    => Exec['systemctl daemon-reload'],
    provider  => systemd,
    before    => $service_before,
    subscribe => $service_subscribe,
  }
}
