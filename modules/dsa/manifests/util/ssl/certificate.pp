# Request a LE signed certificate and put a DANE/TLSA record into DNS.
#
# @param dn
#   The main distinguished name to use for the certificate.
#   Defaults to $name.
# @param subject_alternative_names
#   To request a SAN certificate, pass an array with the
#   alternative names here. The main $dn will be added automatically.
#
# @param tlsa_ports
#   List of DNS ports (on TCP) for which to publish a DANE record for
#   this key.
#
#   If we ever care about UDP ports, we can turn this into an
#   Array[Variant[Stdlib::Port, Tuple[Stdlib::Port, Enum['tcp', 'udp']]]] and
#   allow things like tlsa_ports => [853, 53, [53, 'udp']].
define dsa::util::ssl::certificate (
  Dehydrated::DN $dn = $name,
  Array[Dehydrated::DN] $subject_alternative_names = [],
  Array[Stdlib::Port] $tlsa_ports = [443],
) {
  include dehydrated
  dehydrated::certificate { $name:
    dn                        => $dn,
    subject_alternative_names => $subject_alternative_names,
    algorithm                 => 'prime256v1',
  }

  $key_fingerprint = $facts.dig('dehydrated_domains', $dn, 'fingerprints', 'sha256')
  if $key_fingerprint {
    $dns_content =
      ([$dn] + $subject_alternative_names).unique.map |$dns_name| {
        $tlsa_ports.map |$tlsa_port| {
          "_${tlsa_port}._tcp.${dns_name}. IN TLSA 3 1 1 ${key_fingerprint}"
        }.join("\n")
      }.join("\n")
    dnsextras::entry{ "tlsa-dsa::ssl::certificate[${name}]":
      content => $dns_content,
    }
  }
}
