# = Class: ganeti2::params
#
# Parameters for ganeti clusters
#
# == Sample Usage:
#
#   include ganeti2::params
#
class ganeti2::params {
  include stdlib

  case $::cluster {
    'ganeti2-osuosl.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'pijper.debian.org', 'ipHostNumber')
      $ganeti_priv  = getfromhash($deprecated::allnodeinfo, 'pijper.debian.org', 'ipHostNumber')
      $drbd         = false
    }
    'ganeti.manda.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'manda-node03.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'manda-node04.debian.org', 'ipHostNumber')
      $ganeti_priv  = ['172.29.182.13', '172.29.182.14']
      $drbd         = true
    }
    'ganeti.csail.debian.org': {
      $ganeti_hosts = ['128.31.0.19/32', '128.31.0.20/32']
      $ganeti_priv  = ['172.29.178.0/24']
      $drbd         = true
    }
    'ganeti.grnet.debian.org': {
      $ganeti_hosts = ['194.177.211.197/32', '194.177.211.198/32']
      $ganeti_priv  = ['172.29.175.0/24']
      $drbd         = true
    }
    'ganeti1.conova.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'conova-node-arm05.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'conova-node-arm06.debian.org', 'ipHostNumber')
      $ganeti_priv  = ['172.29.184.11/32', '172.29.184.12/32']
      $drbd         = true
    }
    'ganeti3.ubc.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'ubc-node-arm04.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'ubc-node-arm05.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'ubc-node-arm06.debian.org', 'ipHostNumber')
      $ganeti_priv  = ['172.29.42.102', '172.29.42.112', '172.29.42.122']
      $drbd         = true
    }
    'ganeti2.conova.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'conova-node03.debian.org', 'ipHostNumber') +
                                        getfromhash($deprecated::allnodeinfo, 'conova-node04.debian.org', 'ipHostNumber')
      $ganeti_priv  = ['172.29.185.3/32', '172.29.185.4/32']
      $drbd         = true
    }
    'ganeti4.ubc.debian.org': {
      $ganeti_hosts = getfromhash($deprecated::allnodeinfo, 'ubc-node02.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'ubc-node03.debian.org', 'ipHostNumber') +
                      getfromhash($deprecated::allnodeinfo, 'ubc-node04.debian.org', 'ipHostNumber')
      $ganeti_priv  = ['172.29.42.234', '172.29.42.236', '172.29.42.238']
      $drbd         = true
    }
    default: {
      $ganeti_hosts = []
      $ganeti_priv  = []
      $drbd         = false
    }
  }
}
