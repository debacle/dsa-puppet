# postgres backup server
class postgres::backup_server {
  include postgres::backup_server::globals

  $make_base_backups = '/usr/local/bin/postgres-make-base-backups'

  ensure_packages ( [
    'libhash-merge-simple-perl',
    'libyaml-perl',
    'python3-yaml',
    'pigz',
    'postgresql-client',
  ], {
    ensure => 'installed'
  })

  file { "/home/${postgres::backup_server::globals::backup_unix_user}":
    ensure => directory,
    mode   => '0755',
    owner  => $postgres::backup_server::globals::backup_unix_user,
    group  => $postgres::backup_server::globals::backup_unix_group,
  }

  ####
  # Regularly pull base backups
  #
  concat { $postgres::backup_server::globals::base_backup_clusters:
    ensure_newline => true,
  }
  Concat::Fragment <<| tag == $postgres::backup_server::globals::tag_base_backup |>>

  file { $make_base_backups:
    mode    => '0555',
    content => template('postgres/backup_server/postgres-make-base-backups.erb'),
  }
  file { '/var/lib/dsa/postgres-make-base-backups':
    ensure => directory,
    owner  => $postgres::backup_server::globals::backup_unix_user,
    mode   => '0755',
  }
  concat::fragment { 'puppet-crontab--postgres-make_base_backups':
    target  => '/etc/cron.d/puppet-crontab',
    content => @("EOF")
      */30 * * * * ${postgres::backup_server::globals::backup_unix_user} sleep $(( RANDOM \% 1200 )); chronic ${make_base_backups}
      | EOF
  }

  ####
  # Maintain authorized_keys file on backup servers for WAL shipping
  #
  # do not let other hosts directly build our authorized_keys file,
  # instead go via a script that somewhat validates intput
  file { '/usr/local/bin/debbackup-ssh-wrap':
    source => 'puppet:///modules/postgres/backup_server/debbackup-ssh-wrap',
    mode   => '0555'
  }
  file { '/usr/local/bin/postgres-make-one-base-backup':
    source => 'puppet:///modules/postgres/backup_server/postgres-make-one-base-backup',
    mode   => '0555'
  }
  ssh::authorized_key_collect { 'postgres::backup_server':
    target_user => $postgres::backup_server::globals::backup_unix_user,
    collect_tag => $postgres::backup_server::globals::tag_source_sshkey,
  }

  ####
  # Maintain /etc/nagios/dsa-check-backuppg.conf
  #
  file { '/etc/dsa/postgresql-backup':
    ensure => 'directory',
  }
  file { '/etc/dsa/postgresql-backup/dsa-check-backuppg.conf.d':
    ensure  => 'directory',
    purge   => true,
    force   => true,
    recurse => true,
    notify  => Exec['update dsa-check-backuppg.conf'],
  }
  file { '/etc/dsa/postgresql-backup/dsa-check-backuppg.conf.d/globals.conf':
    content => template('postgres/backup_server/dsa-check-backuppg-globals.conf.erb'),
    notify  => Exec['update dsa-check-backuppg.conf']
  }
  File<<| tag == $postgres::backup_server::globals::tag_dsa_check_backupp |>>
  exec { 'update dsa-check-backuppg.conf':
    command     => @(EOF),
        perl -MYAML=LoadFile,Dump -MHash::Merge::Simple=merge -E 'say Dump(merge(map{LoadFile($_)}@ARGV))' /etc/dsa/postgresql-backup/dsa-check-backuppg.conf.d/*.conf > /etc/nagios/dsa-check-backuppg.conf
        | EOF
    provider    => shell,
    refreshonly => true,
  }

  file { '/etc/sudoers.d/backup-server':
    mode    => '0440',
    content => template('postgres/backup_server/sudoers.erb'),
  }

  ###
  # Expire old backups
  #
  file { '/etc/cron.d/dsa-expire-pg-backups':
    ensure => absent,
  }
  concat::fragment { 'puppet-crontab--expire-pg-backups':
    target  => '/etc/cron.d/puppet-crontab',
    content => @(EOF)
      3 13 * * 2 debbackup /usr/lib/nagios/plugins/dsa-check-backuppg -e
      | EOF
  }

  ####
  # Maintain .pgpass file on backup servers
  # #
  concat { $postgres::backup_server::globals::pgpassfile:
    owner => $postgres::backup_server::globals::backup_unix_user,
    group => $postgres::backup_server::globals::backup_unix_group,
    mode  => '0400'
  }
  Concat::Fragment <<| tag == $postgres::backup_server::globals::tag_source_pgpassline |>>
}
