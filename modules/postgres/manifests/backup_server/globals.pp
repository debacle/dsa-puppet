# Global definitions for the postgres::backup_server setup
#
# @param backup_unix_user      unix user on the backup host
# @param backup_unix_group     group of unix user on the backup host
# @param pgpassfile            pg password file for pg_basebackup runs
# @param base_backup_clusters  where to store the list of clusters to make base backups of
class postgres::backup_server::globals(
  String $backup_unix_user,
  String $backup_unix_group = $backup_unix_user,
  String $pgpassfile = "/home/${backup_unix_user}/.pgpass",
  String $base_backup_clusters = '/etc/dsa/postgresql-backup/base-backup-clusters',
) {
  $tag_base_backup = 'postgresql::server::backup-source-make-base-backup-entry'
  $tag_source_sshkey = 'postgresql::server::backup-source-sshkey'
  $tag_source_pgpassline = 'postgresql::server::backup-source-pgpassline'
  $tag_dsa_check_backupp = 'postgresql::server::backup-dsa-check-backuppg'
}
