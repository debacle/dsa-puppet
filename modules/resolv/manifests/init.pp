class resolv(
  Array[Stdlib::IP::Address] $nameservers = [],
  Array[String] $searchpaths = [],
  Array[String] $resolvoptions = [],
) {

  $ns = $facts['unbound'] ? {
    true    => ['127.0.0.1'],
    default => $nameservers,
  }

  $unboundresolvoptions = (versioncmp($::lsbmajdistrelease, '11') >= 0) ? {
    true    => ['edns0', 'trust-ad'],
    default => ['edns0'],
  }

  $opts = $facts['unbound'] ? {
    true    => $unboundresolvoptions + $resolvoptions,
    default => $resolvoptions,
  }

  file { '/etc/resolv.conf':
      content => template('resolv/resolv.conf.erb');
  }

  file { '/etc/dhcp/dhclient-enter-hooks.d/puppet-no-resolvconf':
    content  => @("EOF"),
                   make_resolv_conf() {
                     :
                   }
                   | EOF
    mode => '555',
    ensure => ($dhclient and $unbound) ? {
      true     => 'present',
      false    => 'absent',
    }
  }
}
