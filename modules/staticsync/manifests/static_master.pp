# static master
#
# each component defines exactly one static master.  Content is copied from the source host
# to the master, and from there to all the mirrors.
#
class staticsync::static_master (
) {
  include staticsync::base
  include staticsync::srvdir

  # masters need to talk to mirrors and sources and themselves
  class { 'staticsync::ssh':
    add_tag     => [ 'staticsync-mirror', 'staticsync-source', 'staticsync-master' ],
    collect_tag => 'staticsync-master',
  }

  file { '/usr/local/bin/static-master-run':
    source => 'puppet:///modules/staticsync/static-master-run',
    mode   => '0555',
  }
  file { '/usr/local/bin/static-master-update-component':
    source => 'puppet:///modules/staticsync/static-master-update-component',
    mode   => '0555',
  }
  concat { '/etc/static-clients.conf':
    ensure_newline => true,
    warn           => @(EOF),
    ## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
    | EOF
  }
  Concat::Fragment <<| tag == 'staticsync::static-mirrors-to-trigger' |>>

  file { "/home/${staticsync::user}/static-master":
    ensure => link,
    target => $staticsync::basedir,
  }
  file { "${staticsync::basedir}/master":
    ensure => directory,
    mode   => '0755',
    owner  => $staticsync::user,
    group  => $staticsync::user,
  }

  $staticsync::components.each |$host, $host_data| {
    if $host_data['master'] == $trusted['certname'] {
      mon::service { "dsa_check_mirrorsync/staticsync - ${host}":
        vars      => merge({
          service_name => $host,
          path         => '.serial',
          unix         => true,
          ssl          => true,
        },
        pick($host_data['check_override_vars'], {}),
        ),
      }
    }
  }
}
