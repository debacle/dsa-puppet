# Sources are systems that produce content that is then shipped to masters (which ship it to mirrors).
# Source can trigger these mirror pulses.
#
# the class doesn't need anything beyond what is in the common base set for now.
class staticsync::static_source (
) {
  include staticsync::base

  if ! defined(Class["staticsync::static_master"]) {
    # sources talk only to masters, but only set this up if we are not
    # *also* a static master since we cannot have two meaningful staticsync:ssh
    # instances in the current setup.
    #
    # this adds the limitation that if a system is both master and source, then
    # any components which have this system as a source need to also use the same
    # system as their master.
    class { 'staticsync::ssh':
      add_tag     => 'staticsync-master',
      collect_tag => 'staticsync-source',
    }
  }
}
