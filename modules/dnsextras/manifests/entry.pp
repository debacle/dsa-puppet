# Add a snippet to a zone file
define dnsextras::entry (
  String $content,
) {
  $zone = 'debian.org'
  @@concat::fragment { "dns-extra-${zone}-${trusted['certname']}-${name}":
    target  => "/srv/dns.debian.org/puppet-extra/include-${zone}",
    content => "; ${::fqdn} ${name}\n${content}\n",
    tag     => 'dnsextra',
  }
}
