define dnsextras::tlsa_record (
	$certfile,
	$hostname,
	$port,
) {
	$snippet = gen_tlsa_entry($certfile, $hostname, $port)
	dnsextras::entry{ "$name":
		content => $snippet,
	}
}
