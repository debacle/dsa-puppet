class autofs::common {
	package { 'autofs': ensure => installed }
	package { 'nfs-common': ensure => installed }

	base::linux_module { 'nfs': }
	base::linux_module { 'nfsv4': }
	base::linux_module { 'autofs4': }

	exec { 'autofs reload':
		path        => '/usr/bin:/usr/sbin:/bin:/sbin',
		command     => 'service autofs reload',
		refreshonly => true,
		require =>  Package['autofs'],
	}


	file { '/etc/auto.master.d':
		ensure  => directory
	}
}
