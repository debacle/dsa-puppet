# our primary nameserver
#
# it will not, by default, open the firewall for requests.
class named::primary inherits named::authoritative {
  include dnsextras::entries

  concat::fragment { 'dsa-named-conf-puppet-misc---local-shared-keys':
    target  => '/etc/bind/named.conf.puppet-misc',
    order   => '020',
    content => @(EOF),
      include "/etc/bind/named.conf.shared-keys";
      | EOF
  }
  concat::fragment { 'dsa-named-conf-puppet-misc---named.conf.external-secondaries-ACLs':
    target  => '/etc/bind/named.conf.puppet-misc',
    order   => '025',
    content => template('named/named.conf.external-secondaries-ACLs.erb'),
  }

  concat::fragment { 'dsa-named-conf-puppet-misc---openpgpkey-zone':
    target  => '/etc/bind/named.conf.puppet-misc',
    order   => '020',
    content => @("EOF"/$)
      // MAINTAIN-KEY: _openpgpkey.debian.org

      zone "_openpgpkey.debian.org" {
        type slave;
        file "db._openpgpkey.debian.org";
        allow-query { any; };
        masters {
          ${ join(getfromhash($deprecated::allnodeinfo, 'kaufmann.debian.org', 'ipHostNumber'), ";") } ;
        };
        allow-transfer {
          127.0.0.1;
          rcode0-ACL;
          dnsnodeapi-ACL;
        };
        also-notify {
          rcode0-masters;
          dnsnodeapi-masters;
        };

        key-directory "/srv/dns.debian.org/var/keys/_openpgpkey.debian.org";
        sig-validity-interval 40 25;
        auto-dnssec maintain;
        inline-signing yes;
      };
      | EOF
  }
  @@ferm::rule::simple { "dsa-bind-from-${::fqdn}":
    tag         => 'named::keyring::ferm',
    description => 'Allow primary access to the keyring master',
    proto       => ['udp', 'tcp'],
    port        => 'domain',
    saddr       => $base::public_addresses,
  }

  concat::fragment { 'puppet-crontab--nsec3':
    target  => '/etc/cron.d/puppet-crontab',
    content => @(EOF)
      13 19 4 * * root chronic /usr/sbin/rndc signing -nsec3param 1 0 16 $(head -c 20 /dev/urandom | sha512sum | cut -b 1-10) debian.net
      29 12 7 * * root chronic /usr/sbin/rndc signing -nsec3param 1 0 16 $(head -c 20 /dev/urandom | sha512sum | cut -b 1-10) debian.org
      32 12 7 * * root chronic /usr/sbin/rndc signing -nsec3param 1 0 16 $(head -c 20 /dev/urandom | sha512sum | cut -b 1-10) debconf.org
      36 12 7 * * root chronic /usr/sbin/rndc signing -nsec3param 1 0 16 $(head -c 20 /dev/urandom | sha512sum | cut -b 1-10) _openpgpkey.debian.org

      | EOF
  }
}
