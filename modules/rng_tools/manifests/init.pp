class rng_tools {
	if $hw_can_hwrng {
		$install = $::debarchitecture != 's390x' and !($::is_virtual and $::virtual == 'kvm')
	} else {
		$install = false
	}

	if $install {
		package { ['rng-tools', 'rng-tools-debian']:
			ensure => purged
		}
		package { 'rng-tools5':
			ensure => installed
		}
		service { 'rngd':
			ensure  => running,
			require => Package['rng-tools5']
		}
	} else {
		package { ['rng-tools', 'rng-tools5', 'rng-tools-debian']:
			ensure => purged
		}
	}
}
